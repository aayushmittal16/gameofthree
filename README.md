# Game of Three

### Goal

The Goal is to implement a game with two independent units – the players –
communicating with each other using an API.
### Description
When a player starts, it incepts a random (whole) number and sends it to the second
player as an approach of starting the game. The receiving player can now always choose
between adding one of {-1, 0, 1} to get to a number that is divisible by 3. Divide it by three. The
resulting whole number is then sent back to the original sender.
The same rules are applied until one player reaches the number 1(after the division).

### How to run this project

Following command starts a Jetty server on localhost port 8080:

```
mvn clean compile exec:java
```

Open multiple URLs to start the game:

```
http://localhost:8080
``` 

Every odd number player will wait until an opponent has been found and the system works for multiple games at the same time.

You can play in `Human - Human`, `Human - AI`, and `AI - AI` modes. In each mode, you have to provide a unique username. Once the game has completed, the session ends and you cannot send any more messages.

### Implementation

I decided to use Websockets because they work well for event based systems such as this one that require bidirectional communication and it typically has lower overhead than its HTTP counterpart since it keeps the connection open and results in lower latency than HTTP for real-time communication applications.

I implemented the server side in `Java` and client side along with its AI functionality in a bare minimum UI setup of `HTML` and `JavaScript`. I decided to make the configuration for choosing a player mode for `Human` and `AI` on the client side as players are clients here. Server side only contains infrastructure to maintain player sessions and apply game rules.

I am using six event types here that share the same abstract class `Event`:

1. *InitialTurnEvent* - Sends an event to the user who should make an initial move.
2. *MoveEvent* - This is an incoming event from the user that contains the move, i.e, the next number to be played.
3. *ResultingNumberEvent* - This is an outgoing event to tell a client what the new resulting number is.
4. *TurnEvent* - This event is used to notify the user who has to make the next move.
5. *WinnerEvent* - This is the last event that happens to tell clients that the game is over and so the session is ended automatically from client side to prevent any further communication.
6. *MessageEvent* - This event is used to wrap up string messages about events such as waiting for an opponent, exiting the game, or some error messages for invalid moves.

These events are used on the client side by AI to make relevant decisions.

I maintain a `PlayerRegistry` that has a waiting queue, a session to username mapping, and a username to player object mapping. It also provides ability to match with an opponent - when you open the game connection, on the server side, a player is polled from the queue and matched with you.

`Game` is more than just a model. It maintains validation logic to apply game rules and reject invalid requests. It has a history of moves (I'm not using that anywhere as of now) and a state to determine whose turn is it.

### What else would I do if I could spend more time on this test

* Write thorough unit and integration tests to test for game rules validation, concurrent games ability, session validation and invalidation; this is a must before it goes to production obviously.
* Support client side logic on `Java` too by writing a `GameClient` class along with an `AIGameClient`.
* Add more logging to log user session information.
* If you decide to explore my VCS history, I just pushed directly on `master` branch. No, I do not work like that on a real project. I use either a Pull Request model or a Gerrit workflow.