var ws;

function connect() {
    var username = document.getElementById("username").value;

    var host = document.location.host;
    var pathname = document.location.pathname;
    var mode = document.querySelector('input[name="mode"]:checked').value == 1 ? 'manual' : 'automatic';
    var lastResultingNumber = 0;

    ws = new WebSocket("ws://localhost:8080/play/" + username);
    ws.onmessage = function(event) {
        var log = document.getElementById("log");
        log.innerHTML += "<span style='display: table;'>" + event.data + "</span>\n";
        if (event.data) {
          var data = JSON.parse(event.data);
          var type = data.type;
          if (type === 'WINNER') {
              ws.close();
          } else if (type === 'TURN') {
              setDisplay('moveDiv', 'block');
              if (mode === 'automatic') {
                  document.getElementById("move").value = lastResultingNumber === 0 ? 1 : getClosestNumDivisibleByThree(lastResultingNumber);
                  send();
              }
          } else if (type === 'INITIAL_TURN') {
              setDisplay('moveDiv', 'block');
              if (mode === 'automatic') {
                  document.getElementById("move").value = getRandom(0, 100);
                  send();
              }
          } else if (type === 'RESULTING_NUMBER') {
              lastResultingNumber = data.resultingNumber;
          }
        }
    };
    ws.onclose = function() {
      setDisplay('moveDiv', 'none');
    };
}

function send() {
    var move = document.getElementById("move").value;
    var json = JSON.stringify({
        "move": move,
        "type": "MOVE"
    });

    ws.send(json);
    setDisplay('moveDiv', 'none');
}

function switchMode(mode) {
    var currentSelected = document.querySelector('input[name="mode"]:checked').value;
    if (mode === 'Human' && currentSelected === 2) {
      document.querySelector('input[name="mode"]:checked').value = 1;
    } else if (mode === 'AI' && currentSelected === 1) {
      document.querySelector('input[name="mode"]:checked').value = 2;
    }
}

function setDisplay(id, value) {
    document.getElementById(id).style.display = value;
}

function getRandom(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getClosestNumDivisibleByThree(num) {
    return [1, 0, -1].find((choice) => {
        if ((num + choice)%3 === 0) {
            return num;
        }
    });
}