package com.takeaway.server;

import com.takeaway.exceptions.AppException;
import com.takeaway.models.Game;
import com.takeaway.models.Player;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class PlayerRegistry {

  private static final Map<String, Player> playersRegistry = new ConcurrentHashMap<>();
  private static final Map<String, String> sessionIdToUsernameMap = new ConcurrentHashMap<>();
  private static final Queue<Player> playersWaiting = new ConcurrentLinkedQueue<>();

  private PlayerRegistry() {
  }

  private static boolean isUsernameTaken(String username) {
    return playersRegistry.containsKey(username);
  }

  public static Player registerPlayer(String sessionId, String username) {
    if (isUsernameTaken(username)) {
      throw new AppException(
          "Username " + username + " is taken. Please choose a different username.");
    }
    Player player = new Player(sessionId, username);
    sessionIdToUsernameMap.put(sessionId, username);
    playersRegistry.put(username, player);
    if (playersWaiting.isEmpty()) {
      playersWaiting.add(player);
    }
    return player;
  }

  public static void deregisterPlayer(String sessionId) {
    String username = sessionIdToUsernameMap.get(sessionId);
    playersRegistry.remove(username);
    sessionIdToUsernameMap.remove(sessionId);
  }

  public static Player getPlayer(String sessionId) {
    String username = sessionIdToUsernameMap.get(sessionId);
    return playersRegistry.get(username);
  }

  public static Optional<Player> getOpponent(Player player) {
    Player opponent = playersWaiting.isEmpty() || playersWaiting.peek().equals(player) ? null
        : playersWaiting.poll();
    if (opponent != null) {
      Game game = new Game(opponent, player);
      opponent.setGame(game);
      player.setGame(game);
    }
    return Optional.ofNullable(opponent);
  }
}
