package com.takeaway.server;

import com.google.gson.Gson;
import com.takeaway.events.Event;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class EventEncoder implements Encoder.Text<Event> {

  private static final Gson gson = new Gson();

  @Override
  public String encode(Event event) {
    return gson.toJson(event);
  }

  @Override
  public void init(EndpointConfig endpointConfig) {
  }

  @Override
  public void destroy() {
  }
}