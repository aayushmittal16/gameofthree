package com.takeaway.server;

import static java.lang.String.format;

import com.takeaway.events.Event;
import com.takeaway.events.Event.TYPE;
import com.takeaway.events.MoveEvent;
import com.takeaway.exceptions.AppException;
import com.takeaway.models.Game;
import com.takeaway.models.Player;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import org.pmw.tinylog.Logger;

@ServerEndpoint(value = "/play/{username}", decoders = EventDecoder.class, encoders = EventEncoder.class)
public class GameServer {

  private static final Map<String, Session> sessionRegistry = new ConcurrentHashMap<>();
  private static final String CONNECTED_TO_PLAYER = "Connected to player: %s";
  private static final String WAITING_FOR_INITIAL_NUMBER_CHOICE = "Waiting for player: %s to pick an initial number to begin the game.";
  private static final String WAITING_FOR_ANOTHER_PLAYER = "Waiting for another player to connect...";
  private static final String EXITING_GAME = "Exiting game now...";
  private static final String TURN_PLAYED = "Player %s played their turn.";
  private static final String INTERNAL_ERROR_OCCURED = "Internal error occured!";

  @OnOpen
  public void onOpen(Session session, @PathParam("username") String username) {
    sessionRegistry.put(session.getId(), session);
    Player player = PlayerRegistry.registerPlayer(session.getId(), username);
    Optional<Player> opponent = PlayerRegistry.getOpponent(player);
    if (opponent.isPresent()) {
      // Opponent is the first player because they queued first
      Player firstPlayer = opponent.get();
      broadcast(Event.of(player.getSessionId(),
          format(CONNECTED_TO_PLAYER, firstPlayer.getUsername())));
      broadcast(Event.of(firstPlayer.getSessionId(),
          format(CONNECTED_TO_PLAYER, player.getUsername())));
      broadcast(Event.of(player.getSessionId(),
          format(WAITING_FOR_INITIAL_NUMBER_CHOICE, firstPlayer.getUsername())));
      broadcast(Event.of(firstPlayer.getSessionId(), firstPlayer.getUsername(), TYPE.INITIAL_TURN));
    } else {
      broadcast(Event.of(player.getSessionId(), WAITING_FOR_ANOTHER_PLAYER));
    }
  }

  @OnMessage
  public void onMessage(Session session, Event event) {
    if (!(event instanceof MoveEvent)) {
      throw new AppException(
          "Illegal event provided. The only allowed event type is MoveEvent");
    }
    MoveEvent moveEvent = (MoveEvent) event;
    Player player = PlayerRegistry.getPlayer(session.getId());
    Game game = player.getGame();
    int resultingNumber = game.addMove(player, moveEvent.getMove());
    String player1SessionId = game.getPlayer1().getSessionId();
    String player2SessionId = game.getPlayer2().getSessionId();
    if (game.isWinner()) {
      broadcastWinner(moveEvent, resultingNumber, player1SessionId, player2SessionId, player);
    } else {
      broadcastMove(moveEvent, resultingNumber, player1SessionId, player2SessionId, player);
    }
  }

  @OnClose
  public void onClose(Session session) throws Exception {
    // Both player sessions should be closed
    if (sessionRegistry.containsKey(session.getId())) {
      Player player = PlayerRegistry.getPlayer(session.getId());
      Player opponent = player.getGame().getOpponent(player);
      closeSession(player.getSessionId());
      closeSession(opponent.getSessionId());
    }
  }

  @OnError
  public void onError(Session session, Throwable throwable) {
    if (throwable instanceof AppException) {
      broadcast(Event.of(session.getId(), throwable.getMessage()));
    } else {
      broadcast(Event.of(session.getId(), INTERNAL_ERROR_OCCURED));
      Logger.error("Internal error occured.\n{}", throwable);
    }
    if (sessionRegistry.containsKey(session.getId())) {
      Player player = PlayerRegistry.getPlayer(session.getId());
      broadcast(Event.of(player.getSessionId(), player.getUsername(),
          player.getGame().getInitialNumber() == null ? TYPE.INITIAL_TURN : TYPE.TURN));
    }
  }

  private void broadcastMove(MoveEvent moveEvent, int resultingNumber, String player1SessionId,
      String player2SessionId,
      Player player) {
    broadcast(Event.of(player1SessionId, format(TURN_PLAYED, player.getUsername())));
    broadcast(Event.of(player2SessionId, format(TURN_PLAYED, player.getUsername())));
    broadcast(Event.of(player1SessionId, resultingNumber, moveEvent));
    broadcast(Event.of(player2SessionId, resultingNumber, moveEvent));
    Player opponent = player.getGame().getOpponent(player);
    broadcast(Event.of(opponent.getSessionId(), opponent.getUsername(), TYPE.TURN));
  }

  private void broadcastWinner(MoveEvent moveEvent, int resultingNumber, String player1SessionId,
      String player2SessionId, Player player) {
    broadcast(Event.of(player1SessionId, resultingNumber, moveEvent));
    broadcast(Event.of(player2SessionId, resultingNumber, moveEvent));
    broadcast(Event.of(player1SessionId, player.getUsername(), TYPE.WINNER));
    broadcast(Event.of(player2SessionId, player.getUsername(), TYPE.WINNER));
  }

  private void broadcast(Event event) {
    if (sessionRegistry.containsKey(event.getSessionId())) {
      try {
        sessionRegistry.get(event.getSessionId()).getBasicRemote().sendObject(event);
      } catch (IOException e) {
        Logger.error("Error communicating to the websocket client with session ID: {}\n{}",
            event.getSessionId(), e);
      } catch (EncodeException e) {
        Logger.error("Error encoding the given event {}\n{}", event, e);
      }
    }
  }

  private void closeSession(String sessionId) throws Exception {
    if (sessionRegistry.containsKey(sessionId)) {
      broadcast(Event.of(sessionId, EXITING_GAME));
      PlayerRegistry.deregisterPlayer(sessionId);
      sessionRegistry.get(sessionId).close();
      sessionRegistry.remove(sessionId);
    }
  }
}
