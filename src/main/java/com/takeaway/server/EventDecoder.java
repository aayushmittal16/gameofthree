package com.takeaway.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;
import com.takeaway.events.Event;
import com.takeaway.events.Event.TYPE;
import com.takeaway.events.InitialTurnEvent;
import com.takeaway.events.MessageEvent;
import com.takeaway.events.MoveEvent;
import com.takeaway.events.ResultingNumberEvent;
import com.takeaway.events.WinnerEvent;
import com.takeaway.exceptions.AppException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

public class EventDecoder implements Decoder.Text<Event> {

  private static final RuntimeTypeAdapterFactory<Event> eventAdapterFactory = RuntimeTypeAdapterFactory
      .of(Event.class, "type")
      .registerSubtype(MessageEvent.class, String.valueOf(TYPE.MESSAGE))
      .registerSubtype(MoveEvent.class, String.valueOf(TYPE.MOVE))
      .registerSubtype(ResultingNumberEvent.class, String.valueOf(TYPE.RESULTING_NUMBER))
      .registerSubtype(WinnerEvent.class, String.valueOf(TYPE.WINNER))
      .registerSubtype(InitialTurnEvent.class, String.valueOf(TYPE.INITIAL_TURN));
  private static final Gson gson = new GsonBuilder().registerTypeAdapterFactory(eventAdapterFactory)
      .create();

  @Override
  public Event decode(String str) {
    try {
      return gson.fromJson(str, Event.class);
    } catch (JsonSyntaxException e) {
      throw new AppException("Invalid number! Please enter a valid number.", e);
    }
  }

  @Override
  public boolean willDecode(String str) {
    return str != null;
  }

  @Override
  public void init(EndpointConfig endpointConfig) {
  }

  @Override
  public void destroy() {
  }
}