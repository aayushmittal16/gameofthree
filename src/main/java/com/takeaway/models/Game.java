package com.takeaway.models;

import com.takeaway.exceptions.AppException;
import java.util.ArrayList;
import java.util.List;

public class Game {

  private enum TURN {ONE, TWO}

  private final Player player1;
  private final Player player2;
  private Integer initialNumber;
  private final List<Integer> moves;
  private TURN turn;


  public Game(Player player1, Player player2) {
    this.player1 = player1;
    this.player2 = player2;
    this.moves = new ArrayList<>();
    this.turn = TURN.ONE;
  }

  public Player getPlayer1() {
    return player1;
  }

  public Player getPlayer2() {
    return player2;
  }

  public int addMove(Player player, int move) {
    validateMove(player, move);
    if (initialNumber == null) {
      initialNumber = move;
      moves.add(move);
    } else if (getLastMove() + move == 1) {
      moves.add(1);
    } else {
      int resultingNumber = (getLastMove() + move) / 3;
      moves.add(resultingNumber);
    }
    switchTurn();
    return getLastMove();
  }

  public Integer getInitialNumber() {
    return initialNumber;
  }

  public boolean isWinner() {
    return getLastMove() == 1;
  }

  public Player getOpponent(Player player) {
    return player.equals(player1) ? player2 : player1;
  }

  private int getLastMove() {
    return moves.get(moves.size() - 1);
  }

  private void switchTurn() {
    turn = turn.equals(TURN.ONE) ? TURN.TWO : TURN.ONE;
  }

  private void validateMove(Player player, int move) {
    if ((player.equals(player1) && turn.equals(TURN.TWO)) || (player.equals(player2) && turn
        .equals(TURN.ONE))) {
      throw new AppException(
          "Invalid move! It is not your turn yet.");
    }
    if (initialNumber == null && move < 0) {
      throw new AppException("Invalid move! Initial number cannot be negative.");
    }
    if (initialNumber != null && (move > 1 || move < -1)) {
      throw new AppException("Invalid move! Your only choices are {-1, 0, 1}.");
    }
    if (initialNumber != null && (getLastMove() + move) != 1 && (getLastMove() + move) % 3 != 0) {
      throw new AppException(
          "Invalid move! Your move did not result in a number divisible by 3.");
    }
  }
}
