package com.takeaway.models;

import java.util.Objects;

public class Player {

  private String sessionId;
  private String username;
  private Game game;

  public Player(String sessionId, String username) {
    this.sessionId = sessionId;
    this.username = username;
  }

  public String getSessionId() {
    return sessionId;
  }

  public String getUsername() {
    return username;
  }

  public Game getGame() {
    return game;
  }

  public void setGame(Game game) {
    this.game = game;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Player player = (Player) o;
    return username.equals(player.username);
  }

  @Override
  public int hashCode() {
    return Objects.hash(username);
  }
}
