package com.takeaway.config;

import com.takeaway.server.GameServer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.websocket.jsr356.server.ServerContainer;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;

public class JettyConfig {

  private static final String ROOT_PATH = "/";
  private static final String BASE_PATH = "./src/main/webapp";
  private static final String[] WELCOME_FILES = new String[]{"index.html"};

  /**
   * Registers a ServerContainer that helps Jetty understand JSR-356 standard (Java Standard
   * Websockets Interface). Also, registers a DefaultServlet at '/' to display users the welcome
   * file - index.html
   *
   * @param server Server
   * @throws Exception
   */
  public static void setupJetty(Server server) throws Exception {
    ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
    context.setContextPath(ROOT_PATH);
    context.setResourceBase(BASE_PATH);
    context.addServlet(DefaultServlet.class, ROOT_PATH);
    context.setWelcomeFiles(WELCOME_FILES);
    server.setHandler(context);
    ServerContainer container = WebSocketServerContainerInitializer.configureContext(context);
    container.addEndpoint(GameServer.class);
  }
}
