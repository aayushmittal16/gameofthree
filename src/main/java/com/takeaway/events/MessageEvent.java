package com.takeaway.events;

public class MessageEvent extends Event {

  private final String message;

  public MessageEvent(String sessionId, String message) {
    super(sessionId, TYPE.MESSAGE);
    this.message = message;
  }
}
