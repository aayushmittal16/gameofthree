package com.takeaway.events;

public class InitialTurnEvent extends Event {

  private String username;

  public InitialTurnEvent(String sessionId, String username) {
    super(sessionId, TYPE.INITIAL_TURN);
    this.username = username;
  }
}
