package com.takeaway.events;

public class WinnerEvent extends Event {

  private final String winner;

  public WinnerEvent(String sessionId, String winnerUsername) {
    super(sessionId, TYPE.WINNER);
    this.winner = winnerUsername;
  }
}
