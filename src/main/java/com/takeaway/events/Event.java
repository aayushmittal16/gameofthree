package com.takeaway.events;

public abstract class Event {

  public enum TYPE {
    MOVE, MESSAGE, WINNER, TURN, INITIAL_TURN, RESULTING_NUMBER
  }

  private final String sessionId;
  private final TYPE type;

  public Event(String sessionId, TYPE type) {
    this.sessionId = sessionId;
    this.type = type;
  }

  public String getSessionId() {
    return sessionId;
  }

  public static Event of(String sessionId, String message) {
    return new MessageEvent(sessionId, message);
  }

  public static Event of(String sessionId, int resultingNumber, MoveEvent moveEvent) {
    return new ResultingNumberEvent(sessionId, resultingNumber, moveEvent);
  }

  public static Event of(String sessionId, String username, TYPE type) {
    if (TYPE.WINNER.equals(type)) {
      return new WinnerEvent(sessionId, username);
    } else if (TYPE.TURN.equals(type)) {
      return new TurnEvent(sessionId, username);
    } else if (TYPE.INITIAL_TURN.equals(type)) {
      return new InitialTurnEvent(sessionId, username);
    }
    return of(sessionId, "Invalid event");
  }
}
