package com.takeaway.events;

public class TurnEvent extends Event {

  private String username;

  public TurnEvent(String sessionId, String username) {
    super(sessionId, TYPE.TURN);
    this.username = username;
  }
}
