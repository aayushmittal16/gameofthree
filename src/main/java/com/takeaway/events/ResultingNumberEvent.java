package com.takeaway.events;

public class ResultingNumberEvent extends Event {

  private final MoveEvent moveEvent;
  private int resultingNumber;

  public ResultingNumberEvent(String sessionId, int resultingNumber, MoveEvent moveEvent) {
    super(sessionId, TYPE.RESULTING_NUMBER);
    this.resultingNumber = resultingNumber;
    this.moveEvent = moveEvent;
  }
}
