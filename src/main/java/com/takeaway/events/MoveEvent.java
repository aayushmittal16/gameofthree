package com.takeaway.events;

public class MoveEvent extends Event {

  private final int move;

  public MoveEvent(String sessionId, int move) {
    super(sessionId, TYPE.MOVE);
    this.move = move;
  }

  public int getMove() {
    return move;
  }
}
