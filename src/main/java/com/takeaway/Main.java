package com.takeaway;

import com.takeaway.config.JettyConfig;
import org.eclipse.jetty.server.Server;

public class Main {

  private static final int PORT = 8080;

  public static void main(String[] args) throws Exception {
    Server server = new Server(PORT);
    JettyConfig.setupJetty(server);
    server.start();
    server.join();
  }
}
